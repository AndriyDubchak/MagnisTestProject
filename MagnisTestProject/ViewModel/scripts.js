var NotesArray = new Array();
var NumberOfNotes;
var CheckedNotes = new Array();
var CurrentUser;
window.onload = function () {
    CreateUser();
    $('#content').load('../View/loginpage.html');
    $('#content').on('click', '#SubmitLogin', function () {
        var UserLogin = localStorage.getItem('UserLogin');
        var UserPassword = localStorage.getItem('UserPassword');
        var InputedLogin = $('#login').val();
        var InputedPassword = $('#password').val();
        var loader;
        if (UserLogin == InputedLogin && UserPassword == InputedPassword) {
            loader = $('#content').load('../View/userpage.html', function () {
                CreateUserGrid();
            });
            var buttonChangeUserdata = $('<button>Change login and password</button>');
            buttonChangeUserdata.addClass('btn');
            buttonChangeUserdata.addClass('btn-info');
            buttonChangeUserdata.addClass('btn-lg');
            buttonChangeUserdata.click(function () {
                var newlogin = $('<input></input>');
                newlogin.addClass('form-control');
                newlogin.attr('placeholder', 'New login');
                var newpassword = $('<input></input>');
                newpassword.addClass('form-control');
                newpassword.attr('placeholder', 'New password');
                newpassword.attr('type', 'password');
                var buttonAccept = $('<button>Change</button>');
                buttonAccept.addClass('btn');
                buttonAccept.addClass('btn-success');
                buttonAccept.click(function () {
                    CurrentUser.changeData(newlogin.val(), newpassword.val());
                    newlogin.remove();
                    newpassword.remove();
                    buttonAccept.remove();
                    buttonCancelChange.remove();
                });
                var buttonCancelChange = $('<button>Cancel</button>');
                buttonCancelChange.addClass('btn');
                buttonCancelChange.addClass('btn-danger');
                buttonCancelChange.click(function () {
                    newlogin.remove();
                    newpassword.remove();
                    buttonAccept.remove();
                    buttonCancelChange.remove();
                });
                $('#LeftSide').append(newlogin);
                $('#LeftSide').append(newpassword);
                $('#LeftSide').append(buttonAccept);
                $('#LeftSide').append(buttonCancelChange);
            });
            var buttonLogOut = $('<button>Log out</button>');
            buttonLogOut.addClass('btn');
            buttonLogOut.addClass('btn-danger');
            buttonLogOut.addClass('btn-lg');
            buttonLogOut.click(function () {
                $('#content').load('../View/loginpage.html');
                buttonChangeUserdata.remove();
                buttonLogOut.remove();
            });
            $('#RightSide').append(buttonChangeUserdata);
            $('#RightSide').append(buttonLogOut);
        }
        else {
            alert('There isn\'t such user');
        }
    });
    $('#content').on('click', '#AddNoteButton', function () {
        $('#UserName').text('1');
        var inputNotation = $('<textarea></textarea>');
        inputNotation.addClass('form-control');
        var inputPriority = $('<input></input>');
        inputPriority.addClass('form-control');
        $('#AddNoteButton').removeClass('active');
        $('#AddNoteButton').addClass('disabled');
        var labelNotation = $('<label>Notatoin:</label>');
        var labelPriority = $('<label>Priority:</label>');
        var buttonConfirm = $('<button>Confirm</button>');
        buttonConfirm.addClass('btn');
        buttonConfirm.addClass('btn-success');
        buttonConfirm.click(function () {
            NumberOfNotes++;
            var username = localStorage.getItem('UserLogin');
            var notation = inputNotation.val();
            var priority = inputPriority.val();
            var CurrentNote = new Note(NumberOfNotes, username, notation, priority);
            NotesArray.push(CurrentNote);
            var col = $('<tr></tr>');
            var checkboxtopublish = $('<td></td>');
            var checkboxbody = $('<input></input>');
            checkboxbody.attr('type', 'checkbox');
            checkboxbody.change(function () {
                if (this.checked) {
                    CheckedNotes.push(CurrentNote);
                    if (CheckedNotes.length > 0) {
                        $('#RemoveNote').removeClass('disabled');
                        $('#RemoveNote').addClass('active');
                        $('#EditNote').removeClass('disabled');
                        $('#EditNote').addClass('active');
                    }
                    else {
                        $('#RemoveNote').removeClass('active');
                        $('#RemoveNote').addClass('disabled');
                        $('#EditNote').removeClass('active');
                        $('#EditNote').addClass('disabled');
                    }
                }
                else {
                    CheckedNotes.splice(CheckedNotes.indexOf(CurrentNote), 1);
                    if (CheckedNotes.length > 0) {
                        $('#RemoveNote').removeClass('disabled');
                        $('#RemoveNote').addClass('active');
                        $('#EditNote').removeClass('disabled');
                        $('#EditNote').addClass('active');
                    }
                    else {
                        $('#RemoveNote').removeClass('active');
                        $('#RemoveNote').addClass('disabled');
                        $('#EditNote').removeClass('active');
                        $('#EditNote').addClass('disabled');
                    }
                }
            });
            checkboxtopublish.append(checkboxbody);
            var idtopublish = $('<td>' + CurrentNote.id + '</td>');
            var authortopublish = $('<td>' + CurrentNote.username + '</td>');
            var timetopublish = $('<td>' + CurrentNote.time + '</td>');
            var notetopublish = $('<td>' + CurrentNote.notation + '</td>');
            var prioritytopublish = $('<td>' + CurrentNote.priority + '</td>');
            col.append(checkboxtopublish);
            col.append(idtopublish);
            col.append(authortopublish);
            col.append(timetopublish);
            col.append(notetopublish);
            col.append(prioritytopublish);
            $('#TableBody').append(col);
            labelNotation.remove();
            inputNotation.remove();
            labelPriority.remove();
            inputPriority.remove();
            buttonConfirm.remove();
            buttonCancel.remove();
            $('#AddNoteButton').removeClass('disabled');
            $('#AddNoteButton').addClass('active');
            CurrentNote.gridDOM = col;
            CurrentNote.notationDOM = notetopublish;
            CurrentNote.priorityDOM = prioritytopublish;
        });
        var buttonCancel = $('<button>Cancel</button>');
        buttonCancel.addClass('btn');
        buttonCancel.addClass('btn-danger');
        buttonCancel.click(function () {
            labelNotation.remove();
            inputNotation.remove();
            labelPriority.remove();
            inputPriority.remove();
            buttonConfirm.remove();
            buttonCancel.remove();
            $('#AddNoteButton').removeClass('disabled');
            $('#AddNoteButton').addClass('active');
        });
        $('#AddBody').append(labelNotation);
        $('#AddBody').append(inputNotation);
        $('#AddBody').append(labelPriority);
        $('#AddBody').append(inputPriority);
        $('#AddBody').append(buttonConfirm);
        $('#AddBody').append(buttonCancel);
        if ($('#AutoSave').is(':checked') == true) {
            SaveData();
        }
    });
    $('#content').on('click', '#RemoveNote', function () {
        for (var i = 0; i < CheckedNotes.length; i++) {
            var DOMelement = CheckedNotes[i].gridDOM;
            DOMelement.remove();
            CheckedNotes.splice(CheckedNotes.indexOf(CheckedNotes[i]), 1);
            NotesArray.splice(NotesArray.indexOf(CheckedNotes[i]), 1);
        }
        $('#RemoveNote').removeClass('active');
        $('#RemoveNote').addClass('disabled');
        if ($('#AutoSave').is(':checked') == true) {
            SaveData();
        }
    });
    $('#content').on('click', '#EditNote', function () {
        $('#EditNote').removeClass('active');
        $('#EditNote').addClass('disabled');
        $('#RemoveNote').removeClass('active');
        $('#RemoveNote').addClass('disabled');
        var labelNotationForEdit = $('<h3>Edit notation with ID ' + CheckedNotes[0].id + '</h3>');
        var inputNotation = $('<textarea></textarea>');
        inputNotation.addClass('form-control');
        var inputPriority = $('<input></input>');
        inputPriority.addClass('form-control');
        $('#AddNoteButton').removeClass('active');
        $('#AddNoteButton').addClass('disabled');
        var labelNotation = $('<label>Notatoin:</label>');
        var labelPriority = $('<label>Priority:</label>');
        var buttonConfirm = $('<button>Confirm</button>');
        buttonConfirm.addClass('btn');
        buttonConfirm.addClass('btn-success');
        buttonConfirm.click(function () {
            CheckedNotes[0].editnote(inputNotation.val(), inputPriority.val());
            CheckedNotes.splice(0, 1);
            if (CheckedNotes.length > 0) {
                labelNotationForEdit.text('Edit notation with ID ' + CheckedNotes[0].id);
            }
            else {
                var AllChechers = $('#content').find('input');
                for (var i = 0; i < AllChechers.length; i++) {
                    var Checker = AllChechers[i];
                    Checker.checked = false;
                    CheckedNotes.splice(CheckedNotes.indexOf(CheckedNotes[i]), 1);
                }
                labelNotationForEdit.remove();
                labelNotation.remove();
                inputNotation.remove();
                labelPriority.remove();
                inputPriority.remove();
                buttonConfirm.remove();
                $('#AddNoteButton').removeClass('disabled');
                $('#AddNoteButton').addClass('active');
            }
        });
        inputNotation.val(CheckedNotes[0].notation);
        inputPriority.val(CheckedNotes[0].priority);
        $('#AddBody').append(labelNotationForEdit);
        $('#AddBody').append(labelNotation);
        $('#AddBody').append(inputNotation);
        $('#AddBody').append(labelPriority);
        $('#AddBody').append(inputPriority);
        $('#AddBody').append(buttonConfirm);
        if ($('#AutoSave').is(':checked') == true) {
            SaveData();
        }
    });
    $('#content').on('click', '#SaveData', function () {
        SaveData();
    });
};
function SaveData() {
    var login = localStorage.getItem('UserLogin');
    var password = localStorage.getItem('UserPassword');
    localStorage.clear();
    localStorage.setItem('UserLogin', login);
    localStorage.setItem('UserPassword', password);
    localStorage.setItem('NumberOfNotes', String(NotesArray.length));
    for (var i = 0; i < NotesArray.length; i++) {
        localStorage.setItem('id' + i, String(NotesArray[i].id));
        localStorage.setItem('username' + i, String(NotesArray[i].username));
        localStorage.setItem('notation' + i, String(NotesArray[i].notation));
        localStorage.setItem('priority' + i, String(NotesArray[i].priority));
    }
}
function CreateUser() {
    if (localStorage.getItem('UserLogin') == null) {
        CurrentUser = new User();
        CurrentUser.changeData('1', '1');
    }
    else {
        CurrentUser = new User();
    }
}
function CreateUserGrid() {
    NumberOfNotes = localStorage.getItem('NumberOfNotes');
    console.log(NumberOfNotes);
    for (var i = 0; i < NumberOfNotes; i++) {
        var id = Number(localStorage.getItem('id' + i));
        var username = localStorage.getItem('username' + i);
        var notation = localStorage.getItem('notation' + i);
        var priority = localStorage.getItem('priority' + i);
        var CurrentNote = new Note(id, username, notation, priority);
        var col = $('<tr></tr>');
        var checkboxtopublish = $('<td></td>');
        var checkboxbody = $('<input></input>');
        checkboxbody.attr('type', 'checkbox');
        checkboxbody.change(function () {
            if (this.checked) {
                CheckedNotes.push(CurrentNote);
                if (CheckedNotes.length > 0) {
                    $('#RemoveNote').removeClass('disabled');
                    $('#RemoveNote').addClass('active');
                    $('#EditNote').removeClass('disabled');
                    $('#EditNote').addClass('active');
                }
                else {
                    $('#RemoveNote').removeClass('active');
                    $('#RemoveNote').addClass('disabled');
                    $('#EditNote').removeClass('active');
                    $('#EditNote').addClass('disabled');
                }
            }
            else {
                CheckedNotes.splice(CheckedNotes.indexOf(CurrentNote), 1);
                if (CheckedNotes.length > 0) {
                    $('#RemoveNote').removeClass('disabled');
                    $('#RemoveNote').addClass('active');
                    $('#EditNote').removeClass('disabled');
                    $('#EditNote').addClass('active');
                }
                else {
                    $('#RemoveNote').removeClass('active');
                    $('#RemoveNote').addClass('disabled');
                    $('#EditNote').removeClass('active');
                    $('#EditNote').addClass('disabled');
                }
            }
        });
        checkboxtopublish.append(checkboxbody);
        var idtopublish = $('<td>' + CurrentNote.id + '</td>');
        var authortopublish = $('<td>' + CurrentNote.username + '</td>');
        var timetopublish = $('<td>' + CurrentNote.time + '</td>');
        var notetopublish = $('<td>' + CurrentNote.notation + '</td>');
        var prioritytopublish = $('<td>' + CurrentNote.priority + '</td>');
        col.append(checkboxtopublish);
        col.append(idtopublish);
        col.append(authortopublish);
        col.append(timetopublish);
        col.append(notetopublish);
        col.append(prioritytopublish);
        $('#TableBody').append(col);
        $('#AddNoteButton').removeClass('disabled');
        $('#AddNoteButton').addClass('active');
        CurrentNote.gridDOM = col;
        CurrentNote.notationDOM = notetopublish;
        CurrentNote.priorityDOM = prioritytopublish;
    }
}
//# sourceMappingURL=scripts.js.map