var Note = (function () {
    function Note(id, username, notation, priority) {
        this.id = id;
        this.username = username;
        this.time = new Date();
        this.notation = notation;
        this.priority = priority;
    }
    Note.prototype.editnote = function (notation, priority) {
        this.notation = notation;
        this.priority = priority;
        this.notationDOM.text(notation);
        this.priorityDOM.text(priority);
    };
    return Note;
}());
//# sourceMappingURL=note.js.map