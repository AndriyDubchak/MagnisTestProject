﻿class Note {
    id: number;
    username: string;
    time: VarDate;
    notation: string;
    priority: string;
    gridDOM;
    notationDOM;
    priorityDOM;
    constructor(id: number, username: string, notation: string, priority: string) {
        this.id = id;
        this.username = username;
        this.time = new Date();
        this.notation = notation;
        this.priority = priority;
    }
    editnote(notation: string, priority: string) {
        this.notation = notation;
        this.priority = priority;
        this.notationDOM.text(notation);
        this.priorityDOM.text(priority);
    }
}